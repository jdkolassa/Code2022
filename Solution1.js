const readline = require('node:readline')
const fs = require('fs')
const lo = require('lodash')

/**
 * OBJECTIVES:
 * 1 - Find the elf carrying the most calories
 * 2 - Find the total number of calories that elf is carrying
 */

const fetchSnackers = () => {
    const results = []
    for (let i = 1; i < 4; i++) {
        let maximum = lo.max(totals)
        results.unshift(maximum)
        lo.pull(totals, maximum)
    }
    return results
}

const allElves = []
const totals = []
let backpack = {
    items: [],
    totalCalories: 0
}
const rl = readline.createInterface({
    input: fs.createReadStream('./data/1_input.txt'),
    terminal: false
})

rl.on('line', (line) => {
    if (line === '') {
       backpack.totalCalories = backpack.items.reduce((acc, cur) => {
           return Number(acc) + Number(cur);
       }, 0)
       totals.push(backpack.totalCalories)
       allElves.push(backpack)
       backpack = {
           items: [],
           totalCalories: 0
       }
    } else {
        console.log('A line!')
        backpack.items.push(line)
    }
    rl.emit('close')
})

console.log('Line 39 comes after the readLine events')



setTimeout(() => {
    const top3 = fetchSnackers()
    console.log(top3)
    // console.log('And the elf with the most calories has: ' + fattestElf + ' calories!')
}, 9999)



